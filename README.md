# Load Balancing
**Definition**: Load balancing is the process of distributing network traffic across multiple servers. Basically its the process of taking large number of requests and balance them over few servers using some algorithms so that requests on all the servers are evenly distributed. It increases the availability of applications and websites for users. In the modern day world, almost all applications require load balancing.

## Algorithms used in Load Balancing
* *Least Connection Method*- The traffic goes to the server with least number of active connections.
* *Least Response Time Method*- The traffic goes to the server with least number of active connections and having the   lowest average response time.
* *Round Robin Method*- The traffic goes to the first available server in a rotation order and then the server moves down in the queue.
* *IP Hash*- Here, the IP address of the client determines which server will recieve the request.
  
# Scalability
The ability to handle more requests by buying more machines or bigger machines is called scalability. \
When we use Bigger machines, it is called **Vertical Scaling**. \
When we use more machines, it is called **Horizontal Scaling**.

## Horizontal Scaling
* It requires implementation of load balancing.
* It is more resiliant as in case of failure of one server the request can be handled by other servers.
* It uses Remote Procedure Calls (RPC).
* Consistency of data is the main issue here. While reading and writting data from different servers we need to provide read and write locks.
* It scales well as number of users increase.
  
## Vertical Scaling
* No implementation load balancing is required.
* It can be failed on a single point so no resiliant.
* It has interprocess communication.
* It provides data consistency.
* The hardware part is limited here unlike horizontal scaling.

# Reference
* [Load balancing](https://avinetworks.com/what-is-load-balancing/)